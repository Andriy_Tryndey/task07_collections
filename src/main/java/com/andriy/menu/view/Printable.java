package com.andriy.menu.view;

public interface Printable {
    void print();
}
