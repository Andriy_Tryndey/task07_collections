package com.andriy.menu.view;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {

    private Map<Enum, String> menu;
    private Map<String, Printable> methodManu;
    private static Scanner sc = new Scanner(System.in);

    public MyView() {
        menu = new LinkedHashMap<>();
        methodManu = new LinkedHashMap<>();

        menu.put(DayOfWeek.Monday, "1. Schedule of lessons for monday");
        menu.put(DayOfWeek.Tuesday, "2. Schedule of lessons for Tuesday");
        menu.put(DayOfWeek.Wednesday, "3. Schedule of lessons for Wednesday");
        menu.put(DayOfWeek.Thursday, "4. Schedule of lessons for Thursday");
        menu.put(DayOfWeek.Friday, "5. Schedule of lessons for Friday");
        menu.put(DayOfWeek.Saturday, "6. Schedule of lessons for Saturday");
        menu.put(DayOfWeek.Sunday, "7. Schedule of lessons for Sunday");
        menu.put(DayOfWeek.Q, " Q = exit");

        methodManu.put("1", this::pressbutton1);
        methodManu.put("2", this::pressbutton2);
        methodManu.put("3", this::pressbutton3);
        methodManu.put("4", this::pressbutton4);
        methodManu.put("5", this::pressbutton5);
        methodManu.put("6", this::pressbutton6);
        methodManu.put("7", this::pressbutton7);
    }

    private void pressbutton1() {
        System.out.println("1. Mathematics\n" +
                "2. Ukrainian language\n" +
                "3. Ukrainian literature\n" +
                "4. Physics\n" +
                "5. English");
    }

    private void pressbutton2() {
        System.out.println("1.English\n" +
                "2. Informatics\n" +
                "3. Foreign Literature\n" +
                "4. Physics\n" +
                "5. Physical education\n" +
                "6. Geometry");
    }

    private void pressbutton3() {
        System.out.println("1. Mathematics\n" +
                "2. Informatics\n" +
                "3. Chemistry\n" +
                "4. Physics\n" +
                "5. English");
    }

    private void pressbutton4() {
        System.out.println("1. Ukrainian language\n" +
                "2. Mathematics\n" +
                "3. Physics\n" +
                "4. Physical education\n" +
                "5. Ukrainian language\n" +
                "6. Geometry");
    }

    private void pressbutton5() {
        System.out.println("1. English\n" +
                "2.Physics\n" +
                "3. Chemistry\n" +
                "4. Mathematics\n" +
                "5. Excursion");
    }

    private void pressbutton6() {
        System.out.println("Weekend");
    }

    private void pressbutton7() {
        System.out.println("Weekend");
    }

    private void outputMenu() {
        System.out.println("\nMENU");
        for (String s : menu.values()) {
            System.out.println(s);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu day.");
            keyMenu = sc.nextLine().toUpperCase();
            try {
                methodManu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
