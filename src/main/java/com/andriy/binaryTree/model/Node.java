package com.andriy.binaryTree.model;

public class Node {
    public int key;
    public String value;
    public Node left;
    public Node right;

    public Node(int key, String value) {
        this.key = key;
        this.value = value;

    }

    public void printNode(){
        System.out.println("Key " + key + "  Data : " + value);
    }
}
