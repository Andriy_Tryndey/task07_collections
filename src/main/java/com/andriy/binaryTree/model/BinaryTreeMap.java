package com.andriy.binaryTree.model;

import java.util.Collection;
import java.util.Comparator;
import java.util.Map;
import java.util.Set;

public class BinaryTreeMap<K extends Comparable, V> implements Map<K, V> {

    private int size = 0;
    private Node<K, V> root;

    private static class Node<K, V> implements Map.Entry<K, V>{
        public K key;
        public V value;
        public Node<K, V> left;
        public Node<K, V> right;
        public int count;

        public Node(K key, V value) {
            this.key = key;
            this.value = value;

        }

        @Override
        public K getKey() { return key; }

        @Override
        public V getValue() { return value; }

        @Override
        public V setValue(V value) {
            V oldValue = this.value;
            this.value = value;
            return null;
        }
    }

    @Override
    public int size() { return size; }

    @Override
    public boolean isEmpty() {
        if (size == 0){
            return true;
        }else {
            return false;
        }
    }

    @Override
    public boolean containsKey(Object key) {
        return false;
    }

    @Override
    public boolean containsValue(Object value) {
        return false;
    }

    @Override
    public V get(Object key) {
        Comparator<? super K> k = (Comparator<? super K>) key;
        Node<K, V> node = root;
        while (node != null){
        }
        return null;
    }

    @Override
    public V put(K key, V value) {
        Node node = new Node(key, value);
        node.key = key;
        node.value = value;
        if (root == null){
            root = node;
        }else {
            Node current = root;
            Node prev = null;
            while (true){
                prev = current;



            }
        }
        return null;
    }

    @Override
    public V remove(Object key) {
        return null;
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> m) {

    }

    @Override
    public void clear() {

    }

    @Override
    public Set<K> keySet() {
        return null;
    }

    @Override
    public Collection<V> values() {
        return null;
    }

    @Override
    public Set<Entry<K, V>> entrySet() {
        return null;
    }
}
