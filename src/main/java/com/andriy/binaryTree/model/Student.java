package com.andriy.binaryTree.model;

public class Student{
    private  int id;
    private String surName;
    private int rating;
    private int number;

    public Student(int id, String surName, int rating, int number) {
        this.id = id;
        this.surName = surName;
        this.rating = rating;
        this.number = number;
    }


    public int CompareTo(Object o) {
        Student newSt = (Student) o;
        if(this.number>newSt.number) return 1;
        else{
            if(this.number<newSt.number) return -1;
            else{
                return 0;
            }
        }
    }

    public int nextNumber(){
        return this.number=(int)(+Math.random()*100);
    }

    public int getNumber() {
        return number;
    }

    public Student setNumber(int number) {
        this.number = number;
        return this;
    }

    public int getId() {
        return id;
    }

    public Student setId(int id) {
        this.id = id;
        return this;
    }

    public String getSurName() {
        return surName;
    }

    public Student setSurName(String surName) {
        this.surName = surName;
        return this;
    }

    public int getRating() {
        return rating;
    }

    public Student setRating(int rating) {
        this.rating = rating;
        return this;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", surName='" + surName + '\'' +
                ", rating=" + rating +
                ", number=" + number +
                '}';
    }
}
